<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->




## Netzwerk Automatisierung
#### Lessions Learned

---
#### Doch eher ein technischer Einblick in was macht ein Netzwerker mit Software Prinzipien.


---
## Agenda
* Warum Netzwerkautomatisierung
* Basics Ansible
* Basics GIT
* Basics Datenstrukturen

---
### Netzwerkautomatisierung
#### Warum
* Wie viele Netzwerkconfigs sind Interssant
* Wie oft kann man eigentlich Tippfehler machen
* Ab wann ist es nur noch "more of the same"
* Welche Änderungen wurden gemacht
* Funktioniert das Netzwerk noch genau so wie gestern

---
## Ansible
#### Ein Open Source Community Projekt maintained von Red Hat . Sehr einfach gehalten und kann von Server Systemen bis zu Netzwerkkomponenten für die Automatisierung verwendet werden. (http://www.ansible.com)

---
## Ansible Facts

* Ist ein Framework zum Konfigurationsmanagement
* Based on Python
* Client / Server Architektur
* kein Agent Notwendig
* YAML und JSON für die Beschreibung der Konfiguration => Playbooks
* Template Engine (Jinja2)
* Zur Kommunikation werden Standard Protokolle verwendet

---
## Ansible Standard Protokolle
  * SSH
  * SNMP
  * WinRM
  * Netconf

---
## Ansible Überblick

![Ansible Überblick Arbeitsweise](_images/ansible-ueberblick.png)

---
## Ansible Komponenten
  * Inventory
  * Playbooks
  * Rollen
  * Module
  * Collections

---
## Ansible Inventory
* Hirachisch
* Static Inventory
* Dynamic Inventory

---
## Ansible Inventory
#### (static)

* INI
* YAML
* JSON

---
## Ansible Inventory
#### (dynamic)

* Script
* Plugin
* Alles was am Ende YAML oder JSON generiert

---
## Ansible Inventory (INI)
#### (Beispiel)

```INI
[localhost]
localhost

[cisco-nxos]
cisco-nxos-1.local ansible_host=cisco-nxos-1.local
cisco-nxos-2.local ansible_host=172.16.1.1
```

---
## Ansible Inventory (YAML)

``` YAML
all:
  children:
    standorte:
      children:
        netzwerk-standort-a:
          children:
            net-standort-a-core:
              hosts:
                devicenxos1:
```

---
## Inventory Netbox
#### Beispiel (Script)

ansible all -i netbox.py -m ping

https://pypi.org/project/ansible-netbox-inventory/
---
## Inventory Netbox
#### Beispiel (Plugin)

ansible-inventory -v --list -i netbox_inventory.yml


``` YAML
plugin: netbox
api_endpoint: http://localhost:8000
validate_certs: True
config_context: False
group_by:
  - device_roles
query_filters:
  - role: network-edge-router
....
```

https://docs.ansible.com/ansible/latest/plugins/inventory/netbox.html

---
## Ansible Inventory
#### Hirachisch

```
├── inventory
│   ├── first-inventory
│   ├── group_vars
│   │   └── all.yml
│   │   └── campus-a.yml
│   │   └── campus-b.yml
│   │   └── campus-c.yml
│   ├── host_vars
│   │   └── devicenxos1.yml
│   └── network-inventory
```
---
## Ansible Inventory
#### Demo

https://gitlab.com/rstumpner/ansible-deploy-example

---
## Ansible Playbooks

#### Eine Auflistung von Tasks die Ausgeführt werden sollen

---
## Ansible Überblick
![Ansible Überblick Arbeitsweise](_images/ansible-ueberblick-termini.png)

---
## Ansible Playbooks
#### Beispiel

``` YAML
- name: Bild Cisco Configuration CLI from Template
  connection: local
  tags:
    - build
    - cli
    - build-cli
  template:
    src: config-cli.j2
    dest: ./build/{{ role_name|basename }}/{{ inventory_hostname }}-{{ role_name|basename }}.conf

```

---
## Ansible Module

In Python geschriebene Erweiterungen (Programmlogik) die in Playbooks und Rollen verwendet werden können

https://docs.ansible.com/ansible/latest/modules/modules_by_category.html

---
## Ansible Rolle

Ein oder mehre Ansible Playbooks , die zu einer wiederverwendbaren Einheit geschnürt werden können

---
## Ansible Rolle
#### Beispiel

https://gitlab.com/rstumpner/cisco-hostname


---
## Ansible Collections
#### Metaebene

Von den Herstellern oder der Community gewartete Pakete (Rollen und Module)

https://galaxy.ansible.com/debops/debops


---
## Ansible Überblick

![Ansible Uberblick Arbeitsweise](_images/ansible-ueberblick.png)

---
## GIT

###### Ist eine verteilte Open Source Versionsverwaltungssoftware (https://git-scm.com/). Bei einer Verteilten Versionsverwaltung hat jeder Mitarbeiter ein lokales Repository mit einer lokalen Versionsgeschichte gespeichert . Diese kann mit einem anderen Teilnehmer oder einem Zentralen Repository abgleichen werden.

---
## GIT Facts
* Verteilte Versionsverwaltung
* jeder Besitzt eine lokale Kopie des Repositorys
* seit 2005
* Open Source
* Initiiert von Linus Torwalds

---
## GIT
![GIT Workflow](_images/git-workflow.png)

---
## GIT Branches
* Eigenständige Version des Repositorys
* Master bleibt unangetastet
---
## GIT Tags
* Kann wie ein Branch benutzt werden
* sozusagen ein Read Only Branch

---
## Demo
* git clone https://gitlab.com/rstumpner/cisco-hostname.git
* Create Issue
* Create Branch
* Git Pull
* Change to Branch
* Push Changes
* Merge to Master
* Tag Version

---
## GIT Cheat Sheet
* git init (Erstellen eines Repositorys)
* git clone https://repository.example.com (Clonen eines Repositorys)
* git add . --all (Hinzufügen von Änderungen)
* git commit -m "changed Code what ?" (Commit Erzeugen)
* git push origin brunch (Brunch an das Repository Übertragen)
* git pull (Änderungen vom Repository holen)
* git show (Zeige Änderungen des letzten Commits)
* git log --oneline
* git brunch  (Erzeugen eines Brunches)
* git tag
* git merge (Merge Request Erzeugen)

---
## Templateing
* CLI
* YANG
* Tests

---
## Template generierung CLI
* Bekannt Cisco Works...
* CLI wird von jedem Netzwerkadmin verstanden
* Kann einfach verglichen werden
* Einfaches Troubleshooting

---
## Jinja2 mit Cisco CLI
#### Beispiel / Simple

```
! Access config
! GigabitEthernet Switchport L2 Access with Vlan
{% for interfaces , value in interfaces.iteritems() %}
{% if value.switchport_mode is defined  %}
{% if value.switchport_mode == "access"  %}
interface {{ interfaces }}
  {% if value.pport is defined or value.hostname is defined %}
  description {{ value.description }} ({{ value.hostname }}) { {{ value.pport }} }
  {% else %}
  description {{ value.description }}
  {% endif %}
  switchport
  switchport mode access
  switchport access vlan {{ value.vlan_id }}
  no shutdown
!
{% endif %}
{% endif %}
{% endfor %}
```

---
## Jinja2 mit Cisco CLI
#### Beispiel / Komplex

```
! Add Static Route
ipv6 route vrf wan {{ value.ipv6 | ipaddr('network'/'prefix') }} 2001:db8:1000::1
ip route vrf wan {{ value.ipv4 | ipaddr('network'/'prefix') }}  172.16.1.1 name {{ value.description }}
```

---
## Datenstrukturen
* JAML
* JSON
* YANG

---
## YAML
##### (Yet Another Markup Language)

* Eine strukturierte Sprache zur Beschreibung von Daten
* Ähnlich XML oder JSON
* aber leichter zu Lesen
* Markdown nur für Daten
* Grundprinzip Key / Value Paare
* mehr unter http://www.yaml.org/start.html

---
## YAML
##### (Beispiel)

```YAML
# Ein Beispiel
name: "Roland Stumpner"
job: "IT Techniker"
employed: true
languages:
  German: Native
  English: Fluent
  Spanish: Novice
  python: Novice
  ruby: Novice
favorite foods:
  - Schweinsbraten
  - Steak
```
---
## YAML
##### (Example Network)

```YAML
# Ein Beispiel für eine Netzwerk Konfigurationsbeschreibung
interfaces:
  TenGigabitEthernet1/1/7:
    description: "Transit: zen-rz-vsscore-c6880-1010"
    switchport_mode: trunk
  GigabitEthernet1/0/1:
    description: "Cust: wlan-ap (HS-1/AH-34e1) {H01} [1Gbit]"
    switchport_mode: access
    vlan_id: 10
```

---
## JSON
##### (Javascript Object Notation)

* ein leichtgewichtiges Datenaustauschformat
* leicht von Menschen lesbar
* noch leichter für Maschinen
* Datentypen können korrekt dargestellt werden

---
## JSON
##### (Beispiel)

```JSON
# Ein Beispiel

{
  "name": "Roland Stumpner",
  "job": "IT Techniker",
  "employed": true,
  "languages": {
    "German": "Native",
    "English": "Fluent",
    "Spanish": "Novice",
    "python": "Novice",
    "ruby": "Novice"
  },
  "favorite foods": [
    "Schweinsbraten",
    "Steak"
  ]
}

* Strings müssen unter Hochkomma stehen
```

---
## JSON
##### (Beispiel)

```JSON
# Ein Beispiel für Netzwerker
{
  "interfaces": {
    "TenGigabitEthernet1/1/7": {
      "description": "Transit: zen-rz-vsscore-c6880-1010",
      "switchport_mode": "trunk"
    },
    "GigabitEthernet1/0/1": {
      "description": "Cust: wlan-ap (HS-1/AH-34e1) {H01} [1Gbit]",
      "switchport_mode": "access",
      "vlan_id": 10
    }
  }
}
```

---
## Ansible Überblick
![Ansible Überblick Arbeitsweise](_images/ansible-ueberblick.png)


---
## More on this
* https://labs.networkreliability.engineering/
* https://labs.networkreliability.engineering/labs/?lessonId=16&lessonStage=1

---
## Links
- Ansible Inventory Demo
  -  https://gitlab.com/rstumpner/ansible-deploy-example
- Inventory Netbox (Script)
  - https://pypi.org/project/ansible-netbox-inventory/
- Inventory Netbox (Plugin)
  - https://docs.ansible.com/ansible/latest/plugins/inventory/netbox.html
- Ansible Rolle Beispiel
  - https://gitlab.com/rstumpner/cisco-hostname
- Ansible Module:
  - https://docs.ansible.com/ansible/latest/modules/modules_by_category.html
- YAML
  - http://www.yaml.org/start.html
- NRE Labs  
  - https://labs.networkreliability.engineering/
- Ansible for Devops
  - https://www.ansiblefordevops.com/
- Network Programmability and Automation
  - http://shop.oreilly.com/product/0636920042082.do
