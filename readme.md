# Aconet TBG 60 Netzwerk-Automatisierung-Lessions-Learned
This is a Gitlab Repository to do an easy Virtual Lab Environment for Ansible ( https://www.ansible.com/ ) Open Source Software.

To Follow the Instrutions of this Virtual Lab:
Open your Browser

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/Netzwerk-Automatisierung-Lessions-Learned/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/Netzwerk-Automatisierung-Lessions-Learned
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 3 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * client (Ubuntu 16.04 )
  * ansible (Ubuntu 16.04 with Ansible installed)

Option 1 Vagrant (https://www.vagrantup.com/) (local):
  * Downlad and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb?_ga=2.85169500.497796412.1511295690-1446073050.1511295690
      * dpkg -i vagrant_2.0.1_x86_64.deb

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-ansible
  * Setup the vLAB Environment with Vagrant
    * cd sam-vlab-devops-ansible
    * cd vlab
    * cd vagrant
    * vagrant up
  * Check the vLAB Setup
    * vagrant status
  * Login to work with a Node
    * vagrant ssh ansible
